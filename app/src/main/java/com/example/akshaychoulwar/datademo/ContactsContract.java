package com.example.akshaychoulwar.datademo;

import android.provider.BaseColumns;

/**
 * Created by akshaychoulwar on 10/29/2017.
 */

public final class ContactsContract {

    private ContactsContract()
    {

    }

    public static class Contact implements BaseColumns
    {
        public static final String TABLE_NAME="contacts_table";

        public static final String COLUMN_NAME_SIRNAME="Sirname";

        public static final String COLUMN_NAME_MOB="Mobile";

    }
}
