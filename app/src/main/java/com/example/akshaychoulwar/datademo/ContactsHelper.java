package com.example.akshaychoulwar.datademo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by akshaychoulwar on 10/29/2017.
 */

public class ContactsHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME="contacts.db";

    //private static final String SQL_CREATE_ENTRIES="CREATE TABLE contacts_table(Sirname TEXT,Mobile INTEGER)";


    private static final String SQL_DELETE_ENTRIES="DROP TABLE IF EXISTS "+ContactsContract.Contact.TABLE_NAME;

    public ContactsHelper(Context context) {
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Log.w("Inside helper",""+ContactsContract.Contact.COLUMN_NAME_SIRNAME);
        String SQL_CREATE_ENTRIES="CREATE TABLE contacts_table ( "+ ContactsContract.Contact.COLUMN_NAME_SIRNAME+" TEXT);";
        db.execSQL(SQL_CREATE_ENTRIES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
}
